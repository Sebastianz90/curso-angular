import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TravellingComponent } from './travelling/travelling.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';

@NgModule({
  declarations: [
    AppComponent,
    TravellingComponent,
    DestinoViajeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
